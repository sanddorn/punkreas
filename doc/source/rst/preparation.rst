****************
Prepare the Data
****************

To use punkreas from the command line, the dataset must be in a specific
format. You can use punkreas to easily bring your dataset in the required
format. Just write a simple script as follows:

.. code-block:: python

    from punkreas.data.datasets import Dataset

    # Specify a target folder path+name for the dataset
    target_folder = "path/to/folder"

    # Write a script to read the filenames of the CT scans
    scans: list[str] = ["my/first/scan", "my/second/scan"]

    # Write a script to read/set the labels of the CT scans
    labels: list = [1, 0]

    # Optionally write a script to read the filenames of image masks
    masks: list = ["my/first/mask", "my/second/mask"]

    dataset = Dataset(scans, labels, masks=masks)
    dataset.to_compressed_folder(target_folder)
