"""This module holds the DatasetComposition class."""
import torch

from ._dataset import Dataset


class DatasetComposition(Dataset):
    """Dataset composition.

    Class for creating a composition of multiple datasets. A dataset
    composition emulates one joined dataset by redirecting the dataset methods
    to the right source dataset. Thereby it acts like a single dataset with
    the content of all source datasets, while the source dataset remain the
    holders of their respective data.
    """

    def __init__(self, datasets: list[Dataset]):
        """Create a new datset composition from multiple datasets.

        Args:
            datasets: List of datasets to compose.
        """
        self._datasets = datasets
        self._datasets_starts = [0]
        for dataset in self._datasets[:-1]:
            self._datasets_starts.append(len(dataset))

    def __len__(self) -> int:
        """Get the size of the dataset.

        Returns:
            length: Size of the dataset.
        """
        return sum([len(dataset) for dataset in self._datasets])

    def __getitem__(self, idx: int) -> tuple[torch.Tensor, torch.Tensor]:
        """Get scans and labels by index.

        Args:
            idx: Index of the sample to return.

        Returns:
            torch_scan: The scan data tensor.
            torch_label: The label tensor.
        """
        for i in range(-1, -(len(self._datasets_starts) + 1), -1):
            if idx >= self._datasets_starts[i]:
                break
        return self._datasets[i][idx - self._datasets_starts[i]]
