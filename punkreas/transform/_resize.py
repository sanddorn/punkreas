"""This module holds the Resize transformation class."""
import typing
from typing import Union

from ._transform import Transform

if typing.TYPE_CHECKING:

    from ..data import ScanImage


class Resize(Transform):
    """Resize medical image to match a certain size."""

    def __init__(
        self,
        output_shape: tuple[int, int, int],
        order: Union[int, None] = None,
    ) -> None:
        """Create a new resize transformation instance.

        Args:
            output_shape: Size of the generated output image.
            order: The order of the spline interpolation, default is 0 if
                image.dtype is bool and 1 otherwise. The order has to be in the
                range 0-5.
        """
        self._output_shape = output_shape
        self._order = order

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Resize image based on desired output shape and interpolation order.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        image.resize(output_shape=self._output_shape, order=self._order)
