"""This module holds the tests for the punkreas.transform._resize module."""
import numpy as np

from punkreas.data import MedicalImage
from punkreas.transform import Resize


def test_resize_init():
    """Test the constructor."""
    tf = Resize((28, 29, 230), 2)
    assert tf._output_shape == (28, 29, 230)
    assert tf._order == 2


def test_resize_call(test_medical_image_dataset):
    """Test the __call__ method."""
    tf = Resize((5, 6, 7), 1)
    scan = MedicalImage.from_path(
        test_medical_image_dataset.scans[0], dtype=np.float64
    )
    assert scan.shape == (512, 512, 110)
    tf(scan, 0)
    assert scan.shape == (5, 6, 7)
