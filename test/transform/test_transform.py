"""This module holds the tests for the punkreas.transform._transform module."""

import pytest

from punkreas.transform._transform import Transform


@pytest.fixture
def test_medical_image_transform():
    """A Transform instance."""
    Transform.__abstractmethods__ = set()

    return Transform()


def test_medical_image_transform_init(
    test_medical_image_transform,
):
    """Test the constructor."""
    Transform()


def test_medical_image_transform_prepare(
    test_medical_image_transform,
):
    """Test the constructor."""
    tf = Transform()
    tf.prepare(None)


def test_medical_image_transform_getitem(
    test_medical_image_transform,
):
    """Test the __getitem__ method."""
    with pytest.raises(NotImplementedError):
        test_medical_image_transform(None, None)
