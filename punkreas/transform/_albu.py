"""This module holds the Albumentation transformation class."""
from typing import TYPE_CHECKING, Any

import albumentations as A

from ._transform import Transform

if TYPE_CHECKING:
    from ..data import ScanImage


class Albumentation(Transform):
    """Create a new albumentations transformation."""

    def __init__(self, name: str, **kwargs: Any):
        """Create a new albumentations transformation instance.

        Args:
            name: Name of the albumentation transformation class.
            **kwargs: Options for the albumentation transformation class.
        """
        self._transform = getattr(A, name)(**kwargs)

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Needs to be overwritten by and actual __call__ method.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        image.array = self._transform(image=image.array)["image"]
