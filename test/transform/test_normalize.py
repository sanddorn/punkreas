"""This module holds the tests for the punkreas.transform._normalize module."""
import numpy as np

from punkreas.data import ScanImage
from punkreas.transform import Normalize


def test_normalize_init():
    """Test the constructor."""
    tf = Normalize((20.0, 40.0))
    assert tf._bounds == (20.0, 40.0)


def test_normalize_call(test_medical_image_dataset):
    """Test the __call__ method."""
    tf = Normalize((-1024.0, -944.0))
    scan = ScanImage.from_path(
        test_medical_image_dataset.scans[0], dtype=np.float64
    )
    assert scan[30, 23, 87] == -1024.0
    assert scan[90, 64, 72] == -944.0
    tf(scan, 0)
    assert scan[30, 23, 87] == 0.0
    assert scan[90, 64, 72] == 1.0
