import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.figure import Figure
from torch import Tensor
from torchmetrics import ConfusionMatrix as ConfusionMatrixBase


class ConfusionMatrix(ConfusionMatrixBase):
    """Confusion matrix metric.

    Override of the similar torchmetrics implementation to return figure
    instead of Tensor. Works with binary, multiclass, and multilabel data.
    Accepts probabilities or logits from a model output or integer class
    values in prediction. Works with multi-dimensional preds and target, but
    it should be noted that additional dimensions will be flattened.

    Args:
        num_classes: Number of classes in the dataset.
        normalize: Normalization mode for confusion matrix. Choose from

            - ``None`` or ``'none'``: no normalization (default)
            - ``'true'``: normalization over the targets (most commonly used)
            - ``'pred'``: normalization over the predictions
            - ``'all'``: normalization over the whole matrix

        threshold:
            Threshold for transforming probability or logit predictions to
            binary (0,1) predictions, in the case of binary or multi-label
            inputs. Default value of 0.5 corresponds to input being
            probabilities.
        multilabel:
            determines if data is multilabel or not.
        compute_on_step:
            Forward only calls ``update()`` and return None if this is set to
            False. default: True
        dist_sync_on_step:
            Synchronize metric state across processes at each ``forward()``
            before returning the value at the step. default: False
        process_group:
            Specify the process group on which synchronization is called.
            default: None (which selects the entire world)
    """

    def __call__(self, preds: Tensor, target: Tensor) -> Figure:
        """Compute the metric.

        Args:
            preds: Predictions tensor.
            target: Target Tensor.
        """
        confusion_matrix = super().__call__(preds, target)
        df_cm = pd.DataFrame(
            confusion_matrix.cpu().numpy().astype(int),
            index=range(self.num_classes),
            columns=range(self.num_classes),
        )
        plt.figure()
        sns.set(font_scale=2.0)
        figure = sns.heatmap(df_cm, annot=True, fmt="g").get_figure()
        plt.close(figure)
        return figure
