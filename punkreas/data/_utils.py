"""This module contains utils for the punkreas.data package."""
import os

import SimpleITK as sitk


def compress_medical_image(source: str, target: str) -> None:
    """Compress a medical image or DICOM series.

    Args:
        source: Source image file or DICOM series folder.
        target: Target image file.
    """

    abs_path = os.path.abspath(source)

    if os.path.isfile(abs_path):
        image = sitk.ReadImage(abs_path)
    elif os.path.isdir(abs_path):
        reader = sitk.ImageSeriesReader()
        dicom_names = reader.GetGDCMSeriesFileNames(source)
        reader.SetFileNames(dicom_names)
        image = reader.Execute()
    else:
        raise ValueError(f"Specified path {input} does not exist!")

    sitk.WriteImage(image, target, useCompression=True)
