"""This module holds the CropToMask transformation class."""
import typing

import numpy as np

from ..data import MaskImage, ScanImage
from ._transform import Transform

if typing.TYPE_CHECKING:
    from ..data import Dataset


class CropToMask(Transform):
    """Crop medical images to the bounding box of their masks."""

    def __init__(
        self, margin: int = None, min_dim: tuple[int, int, int] = None
    ) -> None:
        """Create a new crop to mask transformation instance.

        Args:
            margin: Optional margin to the boundaries of the bounding box.
            min_dim: Minimum dimension of the cropped image.
        """
        self._bounding_boxes: dict[
            int, tuple[tuple[int, int], tuple[int, int], tuple[int, int]]
        ] = {}
        self._margin = margin
        self._min_dim = min_dim

    def prepare(self, dataset: "Dataset") -> None:
        """Prepare transformation instance based on the dataset.

        Reads the bounding box information of the mask images for later
        application to the scan images.

        Args:
            dataset: Medical image dataset to prepare transformation for.
        """
        self._dataset = dataset
        if dataset.masks is None:
            raise RuntimeError("No mask information in dataset.")

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Crop scan image to it's mask bounding box.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        if idx not in self._bounding_boxes:
            img = MaskImage.from_path(
                self._dataset.masks[idx], dtype=np.bool_  # type: ignore
            )
            self._bounding_boxes[idx] = img.bounding_box
            shape = img.shape
            if self._margin is not None:
                self._bounding_boxes[idx] = tuple(  # type: ignore
                    tuple(
                        np.clip(
                            (minv - self._margin, maxv + self._margin),
                            0,
                            shape[i] - 1,
                        )
                    )
                    for i, (minv, maxv) in enumerate(self._bounding_boxes[idx])
                )
            if self._min_dim is not None:
                new_bbox = []
                for i in range(3):
                    minv, maxv = (
                        self._bounding_boxes[idx][i][0],
                        self._bounding_boxes[idx][i][1],
                    )
                    cmargin = int(np.rint(self._min_dim[i] / 2))
                    center = int(np.rint(np.mean([minv, maxv])))
                    minv = max(min(center - cmargin, minv), 0)
                    maxv = min(max(center + cmargin, maxv), shape[i] - 1)
                    new_bbox.append((minv, maxv))
                self._bounding_boxes[idx] = tuple(new_bbox)  # type: ignore

        bbox = self._bounding_boxes[idx]
        image.crop(bbox[0], bbox[1], bbox[2])
