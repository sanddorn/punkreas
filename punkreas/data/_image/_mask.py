"""This module holds the MaskImage class."""
import numpy as np

from ._image import MedicalImage


class MaskImage(MedicalImage):
    """Mask Image.

    Medical image class for mage masks. Provides methods to read, modify and
    export 3D medical image masks. The mask itself is saved in form of a
    numpy array.

    Attributes:
        array: The array containing the image mask data.
    """

    @property
    def bounding_box(
        self,
    ) -> tuple[tuple[int, int], tuple[int, int], tuple[int, int]]:
        """Get the bounding box indices for the mask.

        Determines the smallest possible bounding box around non-background
        voxels in the mask (assuming background voxels are labeled 0).

        Returns:
            x_lim: Start and end index of bounding box in x-direction.
            y_lim: Start and end index of bounding boy in y-direction.
            z_lim: Start and end index of bounding boy in z-direction.
        """
        xmin, xmax = np.where(np.any(self._array, axis=(1, 2)))[0][[0, -1]]
        ymin, ymax = np.where(np.any(self._array, axis=(0, 2)))[0][[0, -1]]
        zmin, zmax = np.where(np.any(self._array, axis=(0, 1)))[0][[0, -1]]

        return (xmin, xmax), (ymin, ymax), (zmin, zmax)

    @property
    def midpoint(self) -> tuple[float, float, float]:
        """Get the midpoint of the mask.

        Determines the midpoint of the non-background voxels in the image
        mask.

        Returns:
            midpoint: Midpoint of the image mask.
        """
        (xmin, xmax), (ymin, ymax), (zmin, zmax) = self.bounding_box
        return ((xmax - xmin) * 0.5, (ymax - ymin) * 0.5, (zmax - zmin) * 0.5)

    def merge(self) -> None:
        """Merge labels.

        Sets non-zero label values to 1.
        """
        self._array[np.where(self._array != 0)] = 1
