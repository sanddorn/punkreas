"""Interface for punkreas training."""
import os
import shutil
from tempfile import TemporaryDirectory
from typing import Mapping, Optional, Union

import optuna
import torch
import yaml
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger
from pytorch_lightning.trainer import Trainer
from pytorch_lightning.utilities.seed import seed_everything
from torch import nn
from torch.utils.data import DataLoader, random_split

from .. import data, loss, metric, model, optimizer, transform
from ._utils import TrainingBase


class TrainingSession:
    """Training session.

    This class contains all attributes to create, validate and run a training
    session on one or multiple datasets.
    """

    _VALID_CONFIG = {
        "data": {
            "datasets": list,
            "training": {
                "share": int,
                "shuffle": bool,
                "batch_size": int,
            },
            "validation": {
                "share": int,
                "shuffle": bool,
                "batch_size": int,
            },
            "test": {
                "share": int,
                "shuffle": bool,
                "batch_size": int,
            },
            "transform": dict,
        },
        "model": [str, dict],
        "loss": dict,
        "optimizer": dict,
        "metrics": dict,
        "num_epochs": int,
        "result_folder": str,
        "log_folder": str,
        "seed": int,
    }

    def __init__(
        self,
        config_file: str,
        num_workers: int = 8,
        gpus: int = None,
        workdir: Optional[str] = None,
    ):
        """Create a new training session.

        Args:
            config_file: Configuration file.
            num_workers: Number of workers for dataloader.
            gpus: Number of gpus to train on or which GPUs to train on.
            workdir: Optional working directory of training session. If not
                specified, the directory of the config file is used.
        """
        # Load configuration file
        config_file = os.path.abspath(config_file)
        if not os.path.isfile(config_file):
            raise ValueError(
                f"Specified configuration file {os.path.relpath(config_file)} "
                "does not exists"
            )
        with open(os.path.abspath(config_file)) as yaml_file:
            self._config = yaml.full_load(yaml_file)

        # Set the remaining members
        if workdir is None:
            self._workdir = os.path.dirname(config_file)
        else:
            self._workdir = os.path.abspath(workdir)
        self._name = os.path.basename(config_file).removesuffix(".yaml")
        self._num_workers = num_workers
        self._gpus = gpus

        # Validate the configuration
        self._validate_config(self._config)

        print(f"Created training session: {self._name}")

    def setup(self) -> None:
        """Setup the training session."""

        print(f"Setting up training session: {self._name}")

        if self._config["seed"] is not None:
            seed_everything(self._config["seed"], workers=True)

        # Convert relative paths in config file to absolute paths
        dataset_paths = [
            self._abspath(f) for f in self._config["data"]["datasets"]
        ]

        # Configure transformations
        transformations = [
            transform.Compose(
                [
                    getattr(transform, name)(**options)
                    for name, options in self._config["data"][
                        "transform"
                    ].items()
                ]
            )
            for _ in range(len(dataset_paths))
        ]

        # Create dataset composition from all datasets
        dataset_composition = data.DatasetComposition(
            [
                data.Dataset.from_compressed_folder(
                    dataset_folder,
                    transformations[i],
                )
                for i, dataset_folder in enumerate(dataset_paths)
            ]
        )

        # Split data into training, valdiation and test data
        train_data, val_data, test_data = random_split(
            dataset_composition,
            [
                self._config["data"]["training"]["share"],
                self._config["data"]["validation"]["share"],
                self._config["data"]["test"]["share"],
            ],
        )

        # Configure dataloaders
        self._train_dataloader: Optional[DataLoader] = DataLoader(
            train_data,
            batch_size=self._config["data"]["training"]["batch_size"],
            shuffle=self._config["data"]["training"]["shuffle"],
            num_workers=self._num_workers,
        )
        if self._config["data"]["validation"]["share"]:
            self._val_dataloader: Optional[DataLoader] = DataLoader(
                val_data,
                batch_size=self._config["data"]["validation"]["batch_size"],
                shuffle=self._config["data"]["validation"]["shuffle"],
                num_workers=self._num_workers,
            )
        else:
            self._val_dataloader = None
        if self._config["data"]["test"]["share"]:
            self._test_dataloader: Optional[DataLoader] = DataLoader(
                test_data,
                batch_size=self._config["data"]["test"]["batch_size"],
                shuffle=self._config["data"]["test"]["shuffle"],
                num_workers=self._num_workers,
            )
        else:
            self._test_dataloader = None

        # Configure the model
        if (
            isinstance(self._config["model"], dict)
            and len(self._config["model"]) > 1
        ):
            raise NotImplementedError("Multiple models not supported.")
        elif isinstance(self._config["model"], dict):
            mdl = getattr(model, list(self._config["model"].keys())[0])(
                **list(self._config["model"].values())[0]
            )
        else:
            mdl = torch.load(self._abspath(self._config["model"]))
            mdl.eval()

        # Configure the loss
        if len(self._config["loss"]) > 1:
            raise NotImplementedError("Multiple losses not supported.")
        loss_inst = getattr(loss, list(self._config["loss"].keys())[0])(
            **list(self._config["loss"].values())[0]
        )

        # Configure the metrics
        metrics: Optional[Mapping[str, nn.Module]] = {}
        for name, opts in self._config["metrics"].items():
            metrics[name] = getattr(metric, name)(**opts)  # type: ignore

        # Configure the optimizer
        if len(self._config["optimizer"]) > 1:
            raise NotImplementedError("Multiple optimizers not supported.")
        optim = getattr(optimizer, list(self._config["optimizer"].keys())[0])

        # Configure training base
        self._base = TrainingBase(
            model=mdl,
            loss=loss_inst,
            optimizer=lambda x: optim(
                x, **list(self._config["optimizer"].values())[0]
            ),
            metrics=metrics,
        )

        # Configure trainer
        trainer_opts = {
            "max_epochs": self._config["num_epochs"],
            "log_every_n_steps": 1,
            "logger": False,
            "gpus": self._gpus,
        }
        if self._config["log_folder"] is not None:
            print("Setup loggers")
            log_folder = self._abspath(self._config["log_folder"])
            if os.path.exists((p := os.path.join(log_folder, self._name))):
                shutil.rmtree(p)
            tb_logger = TensorBoardLogger(
                log_folder,
                name=self._name,
                version="",
                default_hp_metric=False,
            )
            csv_logger = CSVLogger(
                log_folder,
                name=self._name,
                version="",
            )
            tb_logger.log_hyperparams(self._config)
            csv_logger.log_hyperparams(self._config)
            shape = dataset_composition[0][0].shape
            example_input = torch.zeros(
                self._config["data"]["training"]["batch_size"], *shape
            )
            tb_logger.experiment.add_graph(mdl, example_input)

            print(
                "Start tensorboard with command: tensorboard --logdir="
                f"{log_folder}"
            )
            trainer_opts["logger"] = tb_logger

        self.trainer = Trainer(**trainer_opts)

    def _validate_config(self, config: dict) -> None:
        """Validate the configuation dictionary.

        Raises:
            ValueError: If configuration is not valid.
        """

        def _recursive_check(
            test: dict, ref: dict, path: list[str], valid: bool = True
        ) -> bool:

            for key in ref.keys():

                path = path.copy() + [key]
                if key not in test:
                    print(
                        "Mising parameter {} in configuration.".format(
                            " -> ".join(path)
                        )
                    )
                    valid = False

                elif isinstance(ref[key], dict):
                    valid = _recursive_check(
                        test[key], ref[key], path=path, valid=valid
                    )
                else:
                    if isinstance(ref[key], list):
                        if all(
                            [
                                not isinstance(test[key], ref[key][i])
                                and not test[key] is None
                                for i in range(len(ref[key]))
                            ]
                        ):
                            print(
                                "Parameter {} is of type {} != {}.".format(
                                    " -> ".join(path),
                                    [
                                        ref[key][i].__name__
                                        for i in range(len(ref[key]))
                                    ],
                                    test[key].__class__.__name__,
                                )
                            )
                            valid = False
                    else:
                        if (
                            not isinstance(test[key], ref[key])
                            and not test[key] is None
                        ):
                            print(
                                "Parameter {} is of type {} != {}.".format(
                                    " -> ".join(path),
                                    ref[key].__name__,
                                    test[key].__class__.__name__,
                                )
                            )
                            valid = False

            return valid

        if not _recursive_check(config, self._VALID_CONFIG, []):
            raise ValueError("Invalid configuration!")

        dataset_paths = [
            self._abspath(f) for f in self._config["data"]["datasets"]
        ]
        for p in dataset_paths:
            if not os.path.isdir(p):
                raise ValueError(f"Specified dataset {p} is not a directory.")

        if isinstance(self._config["model"], str) and not os.path.isfile(
            p := self._abspath(self._config["model"])
        ):
            raise ValueError(f"Specified model file {p} is not a file.")

        if self._config["log_folder"] is not None and not os.path.isdir(
            p := self._abspath(self._config["log_folder"])
        ):
            raise ValueError(f"Specified log_folder {p} is not a directory.")

        if self._config["result_folder"] is not None and not os.path.isdir(
            p := self._abspath(self._config["result_folder"])
        ):
            raise ValueError(
                f"Specified result_folder {p} is not a directory."
            )

        if self._gpus is not None and not torch.cuda.is_available():
            raise ValueError("GPU selected but cuda is not available.")

    def _abspath(self, path: str) -> str:
        """Cast into absolute path.

        Returns the path itself if already absolute and joins with the workdir
        of the session if path is relative.

        Args:
            path: The path to cast.

        Returns:
            abspath: Casted path.
        """
        if os.path.isabs(path):
            return path
        return os.path.abspath(os.path.join(self._workdir, path))

    def train(self) -> None:
        """Train and validate model based on the train and validation data."""
        self.trainer.fit(
            self._base, self._train_dataloader, self._val_dataloader
        )
        if self._config["result_folder"] is not None:
            torch.save(
                self._base.model,
                os.path.join(
                    self._workdir,
                    self._config["result_folder"],
                    self._name + ".pth",
                ),
            )

    def test(self) -> None:
        """Test model based on the test data."""
        self.trainer.test(self._base, self._test_dataloader)

    def run(self) -> None:
        """Run the training session."""
        print(f"Running training session: {self._name}")
        self.setup()
        self.train()
        self.test()


class OptimizationTrainingSession:
    """Hyperparameter optimization training session.

    This class contains all attributes to create, validate and run a
    hyperparameter optimization session.
    """

    _OPTUNA_FUNCTIONS = {"int": "suggest_int", "float": "suggest_float"}

    def __init__(
        self,
        config_file: str,
        num_workers: int = 8,
        gpus: int = None,
        workdir: Optional[str] = None,
    ):
        """Create a new hyperparameter optimization session.

        Args:
            config_file: Configuration file.
            num_workers: Number of workers for dataloader.
            gpus: Number of gpus to train on or which GPUs to train on.
            workdir: Optional working directory of training session. If not
                specified, the directory of the config file is used.
        """

        # Load the configuration file and check for optimize section
        with open(os.path.abspath(config_file)) as yaml_file:
            self._baseconfig = yaml.full_load(yaml_file)
        if "optimize" not in self._baseconfig:
            raise ValueError(
                "Missing 'optimize' section in configuration file."
            )

        # Extract and optimize settings and delete from baseconfig
        self._opt = self._baseconfig["optimize"].copy()
        for name in ["metric", "direction", "n_trials"]:
            if name not in self._opt:
                raise ValueError(f"Missing key {name} in optimize settings.")
        del self._baseconfig["optimize"]

        # Set attributes
        self._name = os.path.basename(config_file).removesuffix(".yaml")
        self._num_workers = num_workers
        self._gpus = gpus
        self.study = optuna.create_study(
            study_name=self._name, direction=self._opt["direction"]
        )
        if workdir is None:
            self._workdir = os.path.dirname(config_file)
        else:
            self._workdir = workdir

        print(f"Created optimization session: {self._name}")

    @classmethod
    def _suggest_value(
        cls, trial: optuna.trial.Trial, name: str, opt_config: dict
    ) -> Union[int, float]:
        """Suggest a new parameter value based on configuration."""
        if "type" not in opt_config:
            raise ValueError("Missing 'type' in optimize config.")
        return getattr(trial, cls._OPTUNA_FUNCTIONS[opt_config["type"]])(
            name, **{k: opt_config[k] for k in opt_config if k != "type"}
        )

    @classmethod
    def _insert_values(cls, trial: optuna.trial.Trial, config: dict) -> None:
        """Replace 'optimize' placeholders in config with suggested values."""

        def replace_recursively(config: dict) -> None:
            for key, value in config.items():
                if isinstance(value, dict) and "optimize" in value:
                    if len(value) > 1:
                        raise ValueError(
                            "More than one key in optimize setting detected."
                        )
                    config[key] = cls._suggest_value(
                        trial, key, value["optimize"]
                    )
                    print(f"Setting {key}={config[key]} for optimization.")
                elif isinstance(value, dict):
                    replace_recursively(value)

        replace_recursively(config)

    def _objective(self, trial: optuna.trial.Trial) -> Union[int, float]:
        """Define the objective of the optimization session."""

        with TemporaryDirectory() as tmpdir:

            trail_config = self._baseconfig.copy()
            self._insert_values(trial, trail_config)

            trial_config_file = os.path.join(
                tmpdir,
                f"{self.study.study_name}_{str(trial.number)}.yaml",
            )

            with open(trial_config_file, "w") as yaml_file:
                yaml.dump(trail_config, yaml_file, sort_keys=False)

            session = TrainingSession(
                trial_config_file,
                num_workers=self._num_workers,
                gpus=self._gpus,
                workdir=self._workdir,
            )
            session.setup()
            session.train()
            results = session.trainer.logged_metrics.copy()
            session.test()
            results |= session.trainer.logged_metrics

        return results[self._opt["metric"]]

    def optimize(self) -> None:
        """Start the optimization session."""
        self.study.optimize(self._objective, n_trials=self._opt["n_trials"])

    def run(self) -> None:
        """Run the optimization session."""
        print(f"Running optimization session: {self._name}")
        self.optimize()
