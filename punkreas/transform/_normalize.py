"""This module holds the Normalize transformation class."""
import typing

from ._transform import Transform

if typing.TYPE_CHECKING:
    from ..data import ScanImage


class Normalize(Transform):
    """Normalize image data.

    Normalizes the image data based on specified bounds.
    """

    def __init__(self, bounds: tuple[float, float]) -> None:
        """Create a new normalize transformation instance.

        Args:
            bounds: Bounds for normalizing.
        """
        self._bounds = bounds

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Normalize the image.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        image.normalize(self._bounds)
