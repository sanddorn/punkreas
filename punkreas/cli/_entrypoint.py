"""Command line entrypoint for punkreas training and inference."""
import os
from typing import Union

import click
import yaml

from . import OptimizationTrainingSession, TrainingSession
from ._utils import print_full_terminal_width


@click.group()
def punkreas() -> None:
    """The uncompromising pancreatic tumor classifier.

    Pancreatic tumor classifier implemented in PyTorch.
    """
    pass


@click.command()
@click.argument("config", type=click.Path(exists=True))
@click.option(
    "--num_workers",
    type=int,
    help="Number of workers for dataloader.",
    default=0,
    show_default=True,
)
@click.option(
    "--gpus",
    type=int,
    help="Number of gpus to train on or which GPUs to train on.",
    default=None,
    show_default=True,
)
def train(
    config: str,
    num_workers: int = 0,
    gpus: int = None,
) -> None:
    """Punkreas model training."""
    print_full_terminal_width("PUNKREAS TRAINING")
    with open(os.path.abspath(config)) as yaml_file:
        config_dict = yaml.full_load(yaml_file)
    session: Union[TrainingSession, OptimizationTrainingSession]
    if "optimize" in config_dict:
        session = OptimizationTrainingSession(
            config, num_workers=num_workers, gpus=gpus
        )
    else:
        session = TrainingSession(config, num_workers=num_workers, gpus=gpus)

    session.run()


punkreas.add_command(train)


@click.command()
@click.argument("config_folder", type=click.Path(exists=True))
@click.option(
    "--num_workers",
    type=int,
    help="Number of workers for dataloader.",
    default=0,
    show_default=True,
)
@click.option(
    "--gpus",
    type=int,
    help="Number of gpus to train on or which GPUs to train on.",
    default=None,
    show_default=True,
)
def batchtrain(
    config_folder: str,
    num_workers: int = 0,
    gpus: int = None,
) -> None:
    """Punkreas batch model training."""
    print_full_terminal_width("PUNKREAS BATCH TRAINING")
    sessions: list[Union[TrainingSession, OptimizationTrainingSession]] = []
    for config_file in [
        name for name in os.listdir(config_folder) if name.endswith(".yaml")
    ]:
        config_file = os.path.join(config_folder, config_file)

        with open(os.path.abspath(config_file)) as yaml_file:
            config_dict = yaml.full_load(yaml_file)

        if "optimize" in config_dict:
            sessions.append(
                OptimizationTrainingSession(
                    config_file, num_workers=num_workers, gpus=gpus
                )
            )
        else:
            sessions.append(
                TrainingSession(
                    config_file, num_workers=num_workers, gpus=gpus
                )
            )

    for session in sessions:
        try:
            session.run()
        except Exception as err:
            print("An error occured during training session: ", err)


punkreas.add_command(batchtrain)


@click.command()
def classify() -> None:
    """Punkreas image classification."""
    pass


punkreas.add_command(classify)
