"""This module holds the Clip transformation class."""
from typing import TYPE_CHECKING, Optional

from ._transform import Transform

if TYPE_CHECKING:
    from ..data import ScanImage


class Clip(Transform):
    """Clip image data.

    Clips the image data based on specified minimum and maximum values.
    """

    def __init__(
        self, amin: Optional[float] = None, amax: Optional[float] = None
    ) -> None:
        """Create a new clip transformation instance.

        Args:
            amin: Minimum value. Not clipped if None.
            amax: Maximum value. Not clipped if None.
        """
        self._amin, self._amax = amin, amax

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Clip the image.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        image.clip(self._amin, self._amax)
