"""Implementation of MedicalNet by Tencent.

This implementation is widely adopted from :cite:t:`tencent:medicalnet` which
itself is based on :cite:t:`chen:med3d` The code was refactored and provided
with substantial documentation.
"""
from typing import Type, Union

import torch.nn.functional as F
from torch import Tensor, cat, flatten, nn, zeros
from torch.autograd import Variable


class _BasicResidualBuildingBlock(nn.Module):
    """Basic residual building block.

    The block consists of two convolutional layers, each with a kernel size
    of 3x3x3, two batch normalization layers, a ReLU activation function
    and a downsampling shortcut connection. The implementation is mainly
    based on the basic building block from :cite:t:`kaiming:resnet` upscaled to
    3 dimension and provided with an optional dilated convolution based on
    :cite:t:`chen:deeplab`. Padding is configured automatically based on the
    selected dilation.

    Attributes:
        conv1: First 3x3x3 convolutional layer.
        bn1: First 3d batch normalization layer.
        conv2: Second 3x3x3 convolutional layer.
        bn2: Second 3d batch normalization layer.
        relu: Rectified linear unit activation function.
        downsample: Downsampling layer.
    """

    EXPANSION = 1

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        stride: int = 1,
        dilation: int = 1,
        downsample: Union[
            "_ProjectionShortcut", "_PaddingShortcut", None
        ] = None,
    ) -> None:
        """Create a new basic residual building block.

        Args:
            in_channels: Input channels.
            out_channels: Output channels.
            stride: Stride of the convolution.
            dilation: Spacing between kernel elements.
            downsample: Downsampling layer.
        """
        super(_BasicResidualBuildingBlock, self).__init__()

        # First 3x3x3 convolutional layer
        self.conv1 = nn.Conv3d(
            in_channels,
            out_channels,
            kernel_size=3,
            dilation=dilation,
            stride=stride,
            padding=dilation,
            bias=False,
        )

        # First 3d batch normalization layer
        self.bn1 = nn.BatchNorm3d(out_channels)

        # Second 3x3x3 convolutional layer
        self.conv2 = nn.Conv3d(
            out_channels,
            out_channels,
            kernel_size=3,
            dilation=dilation,
            padding=dilation,
            bias=False,
        )

        # Second 3d batch normalization layer
        self.bn2 = nn.BatchNorm3d(out_channels)

        # Activation function and downsampling
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """

        # Apply the first convolution
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        # Apply the second convolution
        out = self.conv2(out)
        out = self.bn2(out)

        # Apply optional downsampling layer
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x

        # Add residual to the output of the convolutional layer
        out += residual
        out = self.relu(out)

        return out


class _BottleneckResidualBuildingBlock(nn.Module):
    """Bottleneck residual building block.

    The block consists of three convolutional layers, with a kernal size of
    1x1x1, 3x3x3 and 1x1x1 respectively, three batch normalization layers,
    a ReLU activation function and a downsampling shortcut connection.
    The implementation is mainly based on the bottleneck building block from
    :cite:t:`kaiming:resnet` upscaled to 3 dimension and provided with an
    optional dilated convolution based on :cite:t:`chen:deeplab`. Padding is
    configured automatically based on the selected dilation.

    Attributes:
        conv1: First 1x1x1 convolutional layer.
        bn1: First 3d batch normalization layer.
        conv2: Second 3x3x3 convolutional layer.
        bn2: Second 3d batch normalization layer.
        conv3: Third 1x1x1 convolutional layer.
        bn3: Third 3d batch normalization layer.
        relu: Rectified linear unit activation function.
        downsample: Downsampling layer.
    """

    EXPANSION = 4

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        stride: int = 1,
        dilation: int = 1,
        downsample: Union[
            "_ProjectionShortcut", "_PaddingShortcut", None
        ] = None,
    ):
        """Create a new bottleneck residual building block.

        Args:
            in_channels: Input channels.
            out_channels: Output channels.
            stride: Stride of the convolution.
            dilation: Spacing between kernel elements.
            downsample: Downsampling layer.
        """
        super(_BottleneckResidualBuildingBlock, self).__init__()

        # First 1x1x1 convolutional layer
        self.conv1 = nn.Conv3d(
            in_channels, out_channels, kernel_size=1, bias=False
        )

        # First 3d batch normalization layer
        self.bn1 = nn.BatchNorm3d(out_channels)

        # Second 3x3x3 convolutional layer
        self.conv2 = nn.Conv3d(
            out_channels,
            out_channels,
            kernel_size=3,
            stride=stride,
            dilation=dilation,
            padding=dilation,
            bias=False,
        )

        # Second 3d batch normalization layer
        self.bn2 = nn.BatchNorm3d(out_channels)

        # Second 1x1x1 convolutional layer
        self.conv3 = nn.Conv3d(
            out_channels,
            out_channels * self.EXPANSION,
            kernel_size=1,
            bias=False,
        )

        # Third 3d batch normalization layer
        self.bn3 = nn.BatchNorm3d(out_channels * self.EXPANSION)

        # Activation function and downsampling
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """

        # Apply the first convolution
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        # Apply the second convolution
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        # Apply the third convolution
        out = self.conv3(out)
        out = self.bn3(out)

        # Apply optional downsampling layer
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x

        # Add residual to the output of the convolutional layer
        out += residual
        out = self.relu(out)

        return out


class _PaddingShortcut(nn.Module):
    """Shortcut for identity mapping with padding.

    The shortcut performs identity mapping, with extra zero entries padded for
    increasing dimensions. This shortcut introduces no extra parameter. The
    implementation is mainly based on the shortcut (A) from
    :cite:t:`kaiming:resnet`.
    """

    def __init__(self, out_channels: int, stride: int):
        """Create a new shortcut instance.

        Args:
            out_channels: Output channels.
            stride: Stride of average pooling.
        """
        super(_PaddingShortcut, self).__init__()

        self._out_channels, self._stride = out_channels, stride

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """
        out = F.avg_pool3d(x, kernel_size=1, stride=self._stride)
        zero_pads = zeros(
            out.shape[0],
            self._out_channels - out.shape[1],
            out.shape[2],
            out.shape[3],
            out.shape[4],
            device=out.device,
        )
        return Variable(cat([out.data, zero_pads], dim=1))


class _ProjectionShortcut(nn.Module):
    """Projection shortcut.

    The projection shortcut is used to match dimensions by a 1x1x1 convolution.
    The implementation is mainly based on the shortcut (B) from
    :cite:t:`kaiming:resnet`.

    Attributes:
        model: Sequence of Conv3d and BatchNorm3d layer.
    """

    def __init__(self, in_channels: int, out_channels: int, stride: int = 1):
        """Create a new shortcut instance.

        Args:
            in_channels: Input channels.
            out_channels: Output channels.
            stride: Stride of average pooling.
        """
        super(_ProjectionShortcut, self).__init__()
        self.model = nn.Sequential(
            nn.Conv3d(
                in_channels,
                out_channels,
                kernel_size=1,
                stride=stride,
                bias=False,
            ),
            nn.BatchNorm3d(out_channels),
        )

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """
        return self.model(x)


class _ResNetLayer(nn.Module):
    """ResNet layer.

    ResNet layer composed of multiple building blocks. The building blocks are
    stacked on top of each other to create the layer. The first building block
    performs the change in channels numbers, wherein the remaining blocks
    have the specified number of out_channels as input as well as output. The
    implementation is mainly based on the convX_X layers from
    :cite:t:`kaiming:resnet`.

    Attributes:
        layer: The layer created from the stacked residual blocks.
    """

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        building_block: Union[
            Type[_BasicResidualBuildingBlock],
            Type[_BottleneckResidualBuildingBlock],
        ],
        num_blocks: int,
        stride: int = 1,
        dilation: int = 1,
        downsample: Union[_PaddingShortcut, _ProjectionShortcut, None] = None,
    ):
        """Create a new residual layer from building blocks.

        Args:
            in_channels: Input channels.
            out_channels: Output channels.
            building_block: Type of block to stack to layer.
            num_blocks: Number of blocks to stack.
            stride: Stride of the convolution.
            dilation: Spacing between kernel elements.
            downsample: Downsampling layer.
        """
        super(_ResNetLayer, self).__init__()

        # Create first building block with downsampling
        layers = [
            building_block(
                in_channels,
                out_channels,
                stride=stride,
                dilation=dilation,
                downsample=downsample,
            )
        ]

        # Stack subsquent building blocks
        for i in range(1, num_blocks):
            layers.append(
                building_block(
                    out_channels * building_block.EXPANSION,
                    out_channels,
                    dilation=dilation,
                )
            )

        # Create a sequential layer from the building blocks
        self.layer = nn.Sequential(*layers)

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """
        return self.layer(x)


class _ResNet(nn.Module):
    """Classification ResNet.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _BUILDING_BLOCK: Union[
        Type[_BasicResidualBuildingBlock],
        Type[_BottleneckResidualBuildingBlock],
    ]
    _LAYER_DIMS: tuple[int, int, int, int]

    def __init__(
        self,
        input_channels: int = 1,
        num_classes: int = 1,
        downsample_type: str = "projection",
    ) -> None:
        """Create a new ResNet model.

        Args:
            input_channels: Number of input channels.
            num_classes: Number of classes to classify.
            downsample_type: Downsampling layer (Choices: "padding" or
                "projection").
        """
        super(_ResNet, self).__init__()

        self.num_classes = num_classes

        # Define first 7x7x7 convolutional layer
        self.conv1 = nn.Conv3d(
            input_channels,
            64,
            kernel_size=7,
            stride=(2, 2, 2),
            padding=(3, 3, 3),
            bias=False,
        )

        # Define initial batch normalization layer
        self.bn1 = nn.BatchNorm3d(64)

        # Define pooling layer
        self.maxpool = nn.MaxPool3d(kernel_size=(3, 3, 3), stride=2, padding=1)

        self._inplanes = 64

        # Define first stacked layer
        self.conv2_x = self._make_layer(
            out_channels=64,
            building_block=self._BUILDING_BLOCK,
            num_blocks=self._LAYER_DIMS[0],
            downsample_type=downsample_type,
        )

        # Define second stacked layer
        self.conv3_x = self._make_layer(
            out_channels=128,
            building_block=self._BUILDING_BLOCK,
            num_blocks=self._LAYER_DIMS[1],
            stride=2,
            downsample_type=downsample_type,
        )

        # Define third stacked layer
        self.conv4_x = self._make_layer(
            out_channels=256,
            building_block=self._BUILDING_BLOCK,
            num_blocks=self._LAYER_DIMS[2],
            dilation=2,
            downsample_type=downsample_type,
        )

        # Define fourth stacked layer
        self.conv5_x = self._make_layer(
            out_channels=512,
            building_block=self._BUILDING_BLOCK,
            num_blocks=self._LAYER_DIMS[3],
            dilation=4,
            downsample_type=downsample_type,
        )

        # Define average pooling layer
        self.avgpool = nn.AdaptiveAvgPool3d(1)

        # Define fully connected layer
        self.fc = nn.Linear(512 * self._BUILDING_BLOCK.EXPANSION, num_classes)

        # Define softmax layer
        if self.num_classes == 1:
            self.softmax: nn.Module = nn.Sigmoid()
        else:
            self.softmax = nn.Softmax(1)

        # Define activation function
        self.relu = nn.ReLU(inplace=True)

    def _make_layer(
        self,
        out_channels: int,
        building_block: Union[
            Type[_BasicResidualBuildingBlock],
            Type[_BottleneckResidualBuildingBlock],
        ],
        num_blocks: int,
        stride: int = 1,
        dilation: int = 1,
        downsample_type: str = "projection",
    ) -> _ResNetLayer:
        """Make a new ResNet layer.

        Args:
            in_channels: Input channels.
            out_channels: Output channels.
            building_block: Type of block to stack to layer.
            num_blocks: Number of blocks to stack.
            stride: Stride of the convolution.
            dilation: Spacing between kernel elements.
            downsample_type: Downsampling layer (Choices: "padding" or
                "projection").
        """
        downsample: Union[_PaddingShortcut, _ProjectionShortcut, None] = None
        if (
            stride != 1
            or self._inplanes != out_channels * self._BUILDING_BLOCK.EXPANSION
        ):
            if downsample_type == "padding":
                downsample = _PaddingShortcut(
                    out_channels * self._BUILDING_BLOCK.EXPANSION, stride
                )
            elif downsample_type == "projection":
                downsample = _ProjectionShortcut(
                    self._inplanes,
                    out_channels * self._BUILDING_BLOCK.EXPANSION,
                    stride,
                )
            else:
                raise ValueError(
                    f"Invalid choice of downsample type {downsample_type}"
                )
        layer = _ResNetLayer(
            in_channels=self._inplanes,
            out_channels=out_channels,
            building_block=building_block,
            num_blocks=num_blocks,
            stride=stride,
            dilation=dilation,
            downsample=downsample,
        )
        self._inplanes = out_channels * self._BUILDING_BLOCK.EXPANSION
        return layer

    def forward(self, x: Tensor) -> Tensor:
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """

        # Apply first convolution, batch normalization and activation function
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)

        # Apply maxpooling and stacked layers
        x = self.maxpool(x)
        x = self.conv2_x(x)
        x = self.conv3_x(x)
        x = self.conv4_x(x)
        x = self.conv5_x(x)

        # Apply average pool, fully connected layer and softmax
        x = self.avgpool(x)
        x = self.fc(x.view(x.size(0), -1))
        x = self.softmax(x)

        if self.num_classes == 1:
            x = flatten(x)

        return x


class ResNet10(_ResNet):
    """Classification ResNet10.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (1, 1, 1, 1)
    _BUILDING_BLOCK = _BasicResidualBuildingBlock


class ResNet18(_ResNet):
    """Classification ResNet18.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (2, 2, 2, 2)
    _BUILDING_BLOCK = _BasicResidualBuildingBlock


class ResNet34(_ResNet):
    """Classification ResNet34.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (3, 4, 6, 3)
    _BUILDING_BLOCK = _BasicResidualBuildingBlock


class ResNet50(_ResNet):
    """Classification ResNet50.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (3, 4, 6, 3)
    _BUILDING_BLOCK = _BottleneckResidualBuildingBlock


class ResNet101(_ResNet):
    """Classification ResNet101.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (3, 4, 23, 3)
    _BUILDING_BLOCK = _BottleneckResidualBuildingBlock


class ResNet152(_ResNet):
    """Classification ResNet152.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (3, 8, 36, 3)
    _BUILDING_BLOCK = _BottleneckResidualBuildingBlock


class ResNet200(_ResNet):
    """Classification ResNet200.

    Residual Convolutional Neural Network mainly adopted from
    :cite:t:`tencent:medicalnet,chen:med3d` with further reference to
    :cite:t:`kaiming:resnet`. Compared to
    :cite:t:`tencent:medicalnet,chen:med3d`, the segmentation layer has been
    removed and replaced by a classification layer, similar to the
    implementation of :cite:t:`kaiming:resnet`. Further, the number of input
    dimensions and output classes (output_dim) have been made configurable.

    Attributes:
        conv1: First 7x7x7 convolutional layer with stride=2 and padding=3.
        bn1: First 3d batch normalization layer.
        maxpool: 3x3x3 maxpooling layer with stride=2 and padding=1.
        conv2_x: First stacked 3d convolutional layer.
        conv3_x: Second stacked 3d convolutional layer with stride=2.
        conv4_x: Third stacked 3d convolutional layer with dilation=2.
        conv5_x: Fourth stacked 3d convolutional layer with dilation=4.
        avgpool: 3d average pooling layer.
        fc: Fully connected layer.
        softmax: Softmax layer.
        relu: Rectified linear unit activation function.
    """

    _LAYER_DIMS = (3, 24, 36, 3)
    _BUILDING_BLOCK = _BottleneckResidualBuildingBlock
