"""This module holds the tests for the punkreas.data.image._mask module."""
import os

import numpy as np
import pytest

from punkreas.data import MaskImage


@pytest.fixture
def test_mask_image(testdata):
    """Test MaskImage object, created from an actual mask image."""
    return MaskImage.from_path(
        os.path.join(testdata, "dataset", "masks", "000000.nii.gz"),
        dtype=np.int64,
    )


def test_mask_image_init(test_mask_image):
    """Test the constructor."""

    assert isinstance(test_mask_image._array, np.ndarray)
    assert test_mask_image._array.shape == (512, 512, 110)
    assert test_mask_image._array[0, 0, 0] == 0
    assert test_mask_image._array[144, 270, 55] == 1
    assert test_mask_image._array[241, 294, 44] == 2


def test_mask_image_bounding_box(test_mask_image):
    """Test the bounding_box property."""

    assert test_mask_image.bounding_box == (
        (144, 298),
        (261, 344),
        (33, 61),
    )


def test_mask_image_bounding_box_midpoint(test_mask_image):
    """Test the midpoint property."""
    assert test_mask_image.midpoint == (77.0, 41.5, 14.0)
