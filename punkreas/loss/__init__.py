"""Package with losses for punkreas training."""
from torch.nn import CrossEntropyLoss
from torch.nn.modules.loss import _Loss as Loss

from ._bceloss import BCELoss

__all__ = ["BCELoss", "CrossEntropyLoss", "Loss"]
