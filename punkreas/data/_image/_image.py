"""This module holds the abstract MedicalImage class."""
import os
from abc import ABC
from typing import Any, Optional, Sequence, Type, TypeVar, Union

import numpy as np
import SimpleITK as sitk
import torch
from skimage.transform import resize

T = TypeVar("T", bound="MedicalImage")

IndexType = tuple[Union[slice, int], Union[slice, int], Union[slice, int]]


class MedicalImage(ABC):
    """Medical Image.

    Base class for medical images. Provides methods to read, modify and export
    3D medical CT images. The image data itself is saved in form of a numpy
    array.
    """

    def __init__(self, array: np.ndarray) -> None:
        """Create a new medical image instance.

        Args:
            array: 3D array containing the medical image data.
        """
        self._array = array

    def __getitem__(self, idx: IndexType) -> Union["MedicalImage", float, int]:
        """Allow indexing of the medical image.

        If all indices are integer values, the method returns the voxel value
        at the indices. If the indices comprise a slice, a new MedicalImage
        object is returned based on the sliced image array.

        Args:
            idx: Indices.
        """
        if any(isinstance(i, slice) for i in idx):
            return MedicalImage(self._array[idx])
        else:
            return self._array[idx]

    @classmethod
    def from_path(cls: Type[T], path: str, dtype: Type[Any]) -> T:
        """Create an medical image instance from an image file path.

        This methods accepts various medical image file formats as well as
        DICOM series.

        Args:
            path: Path to the image file or DICOM folder.

        Returns
            medical_image: Medical image instance.
        """
        abs_path = os.path.abspath(path)

        if os.path.isfile(abs_path):
            image = sitk.ReadImage(abs_path)
        elif os.path.isdir(abs_path):
            reader = sitk.ImageSeriesReader()
            dicom_names = reader.GetGDCMSeriesFileNames(path)
            reader.SetFileNames(dicom_names)
            image = reader.Execute()
        else:
            raise ValueError(f"Specified path {path} does not exist!")
        return cls(cls._get_array(image, dtype=dtype))

    @staticmethod
    def _get_array(sitk_img: sitk.Image, dtype: Type[Any]) -> np.ndarray:
        """Get numpy array from SimpleITK image.

        Args:
            sitk_img: SimpleITK image.
            dtype: Data type of the image data.

        Returns:
            array: Numpy array containing the image data.
        """
        return np.swapaxes(
            np.array(sitk.GetArrayFromImage(sitk_img), dtype=dtype), 0, 2
        )

    @property
    def array(self) -> np.ndarray:
        """Get 3D image data array.

        Returns:
            array: 3D Image data array.
        """
        return self._array

    @array.setter
    def array(self, arr: np.ndarray) -> None:
        """Set 3D image data array.

        Args:
            arr: 3D Image data array.
        """
        self._array = arr

    @property
    def shape(self) -> tuple[int, ...]:
        """Tuple with image dimensions.

        Returns:
            shape: Current dimensions of the medical image.
        """
        return self._array.shape

    def min(self) -> Union[float, int]:
        """Minimum voxel value of the image.

        Returns:
            min_val: Minimum voxel value of the image.
        """
        return np.amin(self._array)

    def max(self) -> Union[float, int]:
        """Maximum voxel value of the image.

        Returns:
            max_val: Maximum voxel value of the image.
        """
        return np.amax(self._array)

    def to_torch(self) -> torch.Tensor:
        """Convert medical image to PyTorch tensor.

        Returns:
            img_tensor: The image tensor.
        """
        return torch.from_numpy(self._array)

    def crop(
        self,
        xlim: tuple[int, int],
        ylim: tuple[int, int],
        zlim: tuple[int, int],
    ) -> None:
        """Crop image in direction by specifiying start and end index.

        The start and end index of limits are included in the cropped image.

        Args:
            xlim: Tuple with start and end index for x direction.
            ylim: Tuple with start and end index for y direction.
            zlim: Tuple with start and end index for z direction.
        """
        self._array = self._array[
            xlim[0] : xlim[1] + 1, ylim[0] : ylim[1] + 1, zlim[0] : zlim[1] + 1
        ]

    def clip(
        self, amin: Optional[float] = None, amax: Optional[float] = None
    ) -> None:
        """Clip (limit) the values in the image.

        Args:
            amin: Minimum value. Not clipped if None.
            amax: Maximum value. Not clipped if None.
        """
        self._array = np.clip(self._array, amin, amax)  # type: ignore

    def pad(
        self,
        pad_width: Union[Sequence, int],
        mode: str = "constant",
        **kwargs: Any,
    ) -> None:
        """Pad the image array.

        pad_width: Number of values padded to the edges of each axis.
            ((before_1, after_1), ... (before_N, after_N)) unique pad widths
            for each axis.((before, after),) yields same before and after pad
            for each axis. (pad,) or int is a shortcut for before = after =
            pad width for all axes.
        mode: One of the following string values or a user supplied function.
            'constant' (default)
                Pads with a constant value.
            'edge'
                Pads with the edge values of array.
            'linear_ramp'
                Pads with the linear ramp between end_value and the
                array edge value.
            'maximum'
                Pads with the maximum value of all or part of the
                vector along each axis.
            'mean'
                Pads with the mean value of all or part of the
                vector along each axis.
            'median'
                Pads with the median value of all or part of the
                vector along each axis.
            'minimum'
                Pads with the minimum value of all or part of the
                vector along each axis.
            'reflect'
                Pads with the reflection of the vector mirrored on
                the first and last values of the vector along each
                axis.
            'symmetric'
                Pads with the reflection of the vector mirrored
                along the edge of the array.
            'wrap'
                Pads with the wrap of the vector along the axis.
                The first values are used to pad the end and the
                end values are used to pad the beginning.
            'empty'
                Pads with undefined values.
        stat_length: Used in 'maximum', 'mean', 'median', and 'minimum'. Number
            of values at edge of each axis used to calculate the statistic
            value. ((before_1, after_1), ... (before_N, after_N)) unique
            statistic lengths for each axis. ((before, after),) yields same
            before and after statistic lengths for each axis. (stat_length,)
            or int is a shortcut for before = after = statistic length for all
            axes. Default is ``None``, to use the entire axis.
        constant_values: Used in 'constant'.  The values to set the padded
            values for each axis.
            ``((before_1, after_1), ... (before_N, after_N))``
            unique pad constants for each axis. ``((before, after),)`` yields
            same before and after constants for each axis. ``(constant,)`` or
            ``constant`` is a shortcut for ``before = after = constant`` for
            all axes. Default is 0.
        end_values: Used in 'linear_ramp'. The values used for the ending value
            of the linear_ramp and that will form the edge of the padded array.
            ``((before_1, after_1), ... (before_N, after_N))`` unique end
            values for each axis. ``((before, after),)`` yields same before and
            after end values for each axis.
            ``(constant,)`` or ``constant`` is a shortcut for
            ``before = after = constant`` for all axes. Default is 0.
        reflect_type: Used in 'reflect', and 'symmetric'.  The 'even' style is
            the default with an unaltered reflection around the edge value. For
            the 'odd' style, the extended part of the array is created by
            subtracting the reflected values from two times the edge value.
        """
        self._array = np.pad(
            self._array,
            pad_width=pad_width,
            mode=mode,
            **kwargs,
        )

    def resize(
        self,
        output_shape: tuple[int, int, int],
        order: Union[int, None] = None,
    ) -> None:
        """Resize image to match a certain size.

        Performs interpolation to up-size or down-size 3-dimensional images.

        Args:
            output_shape: Size of the generated output image.
            order: The order of the spline interpolation, default is 0 if
                image.dtype is bool and 1 otherwise. The order has to be in the
                range 0-5.

        """
        self._array = resize(
            self._array,
            output_shape=output_shape,
            order=order,
            preserve_range=True,
        )
