"""This module holds the CachedDataset class."""
import os

import torch
from alive_progress import alive_bar

from ._dataset import Dataset


class CachedDataset:
    """Cached medical image classification dataset for PyTorch.

    Applies transormations to a dataset and saves the data as pytorch tensors
    for faster loading of data.
    """

    def __init__(self, dataset: Dataset, folder: str) -> None:
        """Create a cached dataset from an image classification dataset.

        Args:
            dataset: The dataset to cache.
            folder: The folder to cache the dataset in.
        """

        # Check that cache folder exists
        folder = os.path.abspath(folder)
        if not os.path.isdir(folder):
            raise ValueError(f"Specified folder {folder} does not exist.")

        self._inps = []
        self._tars = []
        with alive_bar(len(dataset), title="Cache tensors") as bar:
            for i in range(len(dataset)):

                # Get input and target tensor
                inp, tar = dataset[i]
                self._inps.append(os.path.join(folder, f"inp{i:06d}"))
                self._tars.append(os.path.join(folder, f"tar{i:06d}"))

                # Save tensors to file
                torch.save(inp, self._inps[-1])
                torch.save(tar, self._tars[-1])
                bar()

    def __len__(self) -> int:
        """Get the size of the dataset.

        Returns:
            length: Size of the dataset.
        """
        return len(self._inps)

    def __getitem__(self, idx: int) -> tuple[torch.Tensor, torch.Tensor]:
        """Get input and target by index.

        Args:
            idx: Index of the sample to return.

        Returns:
            torch_input: The scan data tensor.
            torch_target: The label tensor.
        """
        return torch.load(self._inps[idx]), torch.load(self._tars[idx])
