********
punkreas
********

Punkreas is an open source software project aimed at providing a generic, well
documented machine learning library for training with medical images. Most
present machine learning projects are build for a specific task. They are
comprised of algorithms that are tailored to some project's needs. Reuse of
software components in other projects is often impaired by limited transferabiliy
of the code. Another common problem is, that the software itself is poorely
documented and badly comprehensible. To really provide reusable algorithms,
the source code needs to be provided with information on **how** the code works
and (when the code is taken from a source or related to a piece of literature)
**where** it comes from.

In oder to resolve the issues mentioned above, punkreas places special
requirements on the design and documentation of its compononents. Everything
that is implemented should be easly transferable, or (preferably) easily
**applicable** and **expandable** to other tasks. More information about the
design of punkreas can be found under **About > Design**.

The project "punkreas" is a work-in-progress and a concept of a machine
learning project that can hopefully meet the needs of more and more medical
machine learning tasks over the time. Feel free to contribute.

.. toctree::
   :maxdepth: 2
   :caption: About

   rst/bibliography

.. toctree::
   :maxdepth: 2
   :caption: Usage

   rst/installation
   rst/preparation
   rst/configuration
   rst/training

.. toctree::
   :maxdepth: 4
   :name: Package
   :hidden:
   :caption: Package

   modules

