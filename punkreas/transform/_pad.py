"""This module holds the Pad transformation class."""
from typing import TYPE_CHECKING, Any

import numpy as np

from ._transform import Transform

if TYPE_CHECKING:
    from ..data import ScanImage


class Pad(Transform):
    """Pad image data.

    Pads the image data to the specified size.
    """

    def __init__(
        self,
        output_shape: tuple[int, int, int],
        mode: str = "constant",
        **kwargs: Any
    ) -> None:
        """Create a new pad transformation instance.

        Args:
            output_shape: Size of the generated output image.
            mode: One of the following string values or a user supplied
                function.
                'constant' (default)
                    Pads with a constant value.
                'edge'
                    Pads with the edge values of array.
                'linear_ramp'
                    Pads with the linear ramp between end_value and the
                    array edge value.
                'maximum'
                    Pads with the maximum value of all or part of the
                    vector along each axis.
                'mean'
                    Pads with the mean value of all or part of the
                    vector along each axis.
                'median'
                    Pads with the median value of all or part of the
                    vector along each axis.
                'minimum'
                    Pads with the minimum value of all or part of the
                    vector along each axis.
                'reflect'
                    Pads with the reflection of the vector mirrored on
                    the first and last values of the vector along each
                    axis.
                'symmetric'
                    Pads with the reflection of the vector mirrored
                    along the edge of the array.
                'wrap'
                    Pads with the wrap of the vector along the axis.
                    The first values are used to pad the end and the
                    end values are used to pad the beginning.
                'empty'
                    Pads with undefined values.
            stat_length: Used in 'maximum', 'mean', 'median', and 'minimum'.
                Number of values at edge of each axis used to calculate the
                statistic value. ((before_1, after_1), ... (before_N, after_N))
                unique statistic lengths for each axis. ((before, after),)
                yields same before and after statistic lengths for each axis.
                (stat_length,) or int is a shortcut for before = after =
                statistic length for all axes. Default is ``None``, to use the
                entire axis.
            constant_values: Used in 'constant'.  The values to set the padded
                values for each axis.
                ``((before_1, after_1), ... (before_N, after_N))``
                unique pad constants for each axis. ``((before, after),)``
                yields same before and after constants for each axis.
                ``(constant,)`` or ``constant`` is a shortcut for ``before =
                after = constant`` for all axes. Default is 0.
            end_values: Used in 'linear_ramp'. The values used for the ending
                value of the linear_ramp and that will form the edge of the
                padded array.
                ``((before_1, after_1), ... (before_N, after_N))`` unique end
                values for each axis. ``((before, after),)``
                yields same before and after end values for each axis.
                ``(constant,)`` or ``constant`` is a shortcut for
                ``before = after = constant`` for all axes. Default is 0.
            reflect_type: Used in 'reflect', and 'symmetric'.  The 'even' style
                is the default with an unaltered reflection around the edge
                value. For the 'odd' style, the extended part of the array is
                created by subtracting the reflected values from two times the
                edge value.

        """
        self._output_shape, self._mode, self._kwargs = (
            output_shape,
            mode,
            kwargs,
        )

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Pad the image.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        image_shape = image.shape
        pad_width = np.array(
            [self._output_shape[i] - image_shape[i] for i in range(3)]
        )
        pad_before = np.floor(pad_width / 2.0).astype(int)
        pad_after = pad_before + pad_width % 2
        image.pad(
            pad_width=[(pad_before[i], pad_after[i]) for i in range(3)],
            mode=self._mode,
            **self._kwargs,
        )
