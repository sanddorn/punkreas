"""This module holds the tests for the punkreas.transform._crop module."""
import numpy as np

from punkreas.data import ScanImage
from punkreas.transform import CropToMask


def test_croptomask_prepare_and_call(test_medical_image_dataset):
    """Test the prepare and __call__ method."""
    tf = CropToMask()
    assert tf._bounding_boxes == {}
    tf.prepare(test_medical_image_dataset)
    scan0 = ScanImage.from_path(
        test_medical_image_dataset.scans[0], dtype=np.float64
    )
    scan1 = ScanImage.from_path(
        test_medical_image_dataset.scans[1], dtype=np.float64
    )
    assert scan0.shape == (512, 512, 110)
    assert scan1.shape == (512, 512, 107)
    tf(scan0, 0)
    tf(scan1, 1)
    assert scan0.shape == (155, 84, 29)
    assert scan1.shape == (211, 124, 35)
