"""This module holds the tests for the punkreas.transform._compose module."""
import numpy as np

from punkreas.data import ScanImage
from punkreas.transform import Compose, CropToMask, Normalize


def test_compose_prepare_and_call(test_medical_image_dataset):
    """Test the prepare and __call__ method."""
    comp = Compose([CropToMask(), Normalize((-67.0, 107.0))])
    comp.prepare(test_medical_image_dataset)
    scan = ScanImage.from_path(
        test_medical_image_dataset.scans[0], dtype=np.float64
    )
    comp(scan, 0)
    assert scan.shape == (155, 84, 29)
    assert scan[30, 23, 25] == 1.0
    assert scan[90, 64, 13] == 0.0
