"""This module holds the Dataset class."""
import json
import os
import typing
from typing import Any, Type, Union

import numpy as np
import torch

from .. import ScanImage
from .._utils import compress_medical_image

if typing.TYPE_CHECKING:
    from ...transform import Transform


class Dataset(torch.utils.data.Dataset):
    """Medical image classification dataset for PyTorch.

    Base class for custom medical classification datasets. Provides methods for
    loading medical images as a Pytorch dataset. This class can be inherited to
    define custom dataset classes for specific datasets.

    Attributes:
        scans: List of the paths to the medical scan images.
        labels: List of labels corresponding to the scans.
        masks: (Optional) List of the paths to the medical mask images.
    """

    def __init__(
        self,
        scans: list[str],
        labels: list,
        transform: "Transform" = None,
        scan_dtype: Type[Any] = np.float32,
        label_dtype: Type[Any] = np.int64,
        masks: Union[list[str], None] = None,
    ) -> None:
        """Create a medical image classification dataset from scans and labels.

        Use a list of file paths for scans and corresponding labels to create a
        medical image dataset.

        Args:
            scans: List of paths to the medical scans.
            labels: List of the labels corresponding to the medical scans.
            transform: Transformation applied the scan images.
            scan_dtype: Data type of scan data.
            label_dtype: Data type of label data.
            masks: Optional paths to masks corresponding the medical scans.
        """

        if not len(scans) == len(labels):
            raise ValueError(
                "Number of scans and labels does not match! {} != {}".format(
                    len(scans), len(labels)
                )
            )

        if (masks is not None) and (len(masks) != len(scans)):
            raise ValueError(
                "Number of scans and masks does not match! {} != {}".format(
                    len(scans), len(masks)
                )
            )

        # Save scans, labels and masks as public attributes
        self.scans = scans
        self.labels = labels
        self.masks = masks

        # Save transformation and dtypes as private attribute
        self._transform = transform
        self._scan_dtype = scan_dtype
        self._label_dtype = label_dtype

        # Prepare transformation instances on the dataset
        if self._transform:
            self._transform.prepare(self)

    def __len__(self) -> int:
        """Get the size of the dataset.

        Returns:
            length: Size of the dataset.
        """
        return len(self.scans)

    def __getitem__(self, idx: int) -> tuple[torch.Tensor, torch.Tensor]:
        """Get scans and labels by index.

        Args:
            idx: Index of the sample to return.

        Returns:
            torch_scan: The scan data tensor.
            torch_label: The label tensor.
        """

        # Select the sample
        scan, label = (
            ScanImage.from_path(self.scans[idx], dtype=self._scan_dtype),
            self.labels[idx],
        )

        # Apply transformation to scan image
        if self._transform:
            self._transform(scan, idx)

        # Tranform scan and label to pytorch tensors
        torch_scan, torch_label = scan.to_torch(), torch.from_numpy(
            np.array(label, dtype=self._label_dtype)
        )

        return torch_scan, torch_label

    def to_compressed_folder(self, target: str) -> None:
        """Export dataset to a compressed folder.

        Creates a folder with subfolders for scans and masks (if provided).
        Converts every medical image to a compressed NIFTI file. Can be
        import via the "from_compressed_folder" method.

        Args:
            target: Target folder to create.
        """

        abs_path = os.path.abspath(target)

        if not os.path.isdir(os.path.dirname(abs_path)):
            raise ValueError("Parent folder of target does not exist.")
        if os.path.isdir(abs_path):
            raise ValueError("Specified target already exists.")

        # Compress scans
        scan_folder = os.path.join(abs_path, "scans")
        os.makedirs(scan_folder)
        for i, scan in enumerate(self.scans):
            target = os.path.join(scan_folder, f"{i:06d}" + ".nii.gz")
            compress_medical_image(scan, target)

        # Save labels
        label_data = {}
        for i, label in enumerate(self.labels):
            label_data[f"{i:06d}"] = label
        label_file = os.path.join(abs_path, "labels.json")
        with open(label_file, "w") as json_file:
            json.dump(label_data, json_file)

        # Compress masks
        if self.masks:
            mask_folder = os.path.join(abs_path, "masks")
            os.makedirs(mask_folder)
            for i, scan in enumerate(self.masks):
                target = os.path.join(mask_folder, f"{i:06d}" + ".nii.gz")
                compress_medical_image(scan, target)

    @classmethod
    def from_compressed_folder(
        cls,
        folder: str,
        transform: "Transform" = None,
        scan_dtype: Type[Any] = np.float32,
        label_dtype: Type[Any] = np.int64,
    ) -> "Dataset":
        """Create a dataset from compressed dataset folder.

        Creates a new dataset instance from a folder created by the method
        "to_compressed_folder".

        Args:
            folder: Compressed dataset folder.
            transform: Transformation applied the scan images.
            scan_dtype: Data type of scan data.
            label_dtype: Data type of label data.
        """
        abs_path = os.path.abspath(folder)

        if not os.path.isdir(abs_path):
            raise ValueError("Specified path must be a directory.")

        # Read scan filenames
        scans: list[str] = []
        for i, filename in enumerate(
            sorted(
                name
                for name in os.listdir(os.path.join(abs_path, "scans"))
                if not name.startswith(".")
            )
        ):
            tag = filename.split(".")[0]
            if int(tag) != i:
                raise RuntimeError("Scans are not consecutive!")
            scans.append(os.path.join(abs_path, "scans", filename))

        # Read labels
        label_file = os.path.join(abs_path, "labels.json")
        with open(label_file) as json_file:
            label_data = json.load(json_file)
        labels = [label_data[f"{i:06d}"] for i in range(len(scans))]

        # Read mask filenames
        if os.path.exists(os.path.join(abs_path, "masks")):
            masks: list[str] = []
            for i, filename in enumerate(
                sorted(
                    name
                    for name in os.listdir(os.path.join(abs_path, "masks"))
                    if not name.startswith(".")
                )
            ):
                tag = filename.split(".")[0]
                if int(tag) != i:
                    raise RuntimeError("Masks are not consecutive!")
                masks.append(os.path.join(abs_path, "masks", filename))

            return cls(
                scans, labels, transform, scan_dtype, label_dtype, masks
            )

        return cls(scans, labels, transform, scan_dtype, label_dtype, None)
