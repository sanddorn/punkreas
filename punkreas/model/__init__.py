"""Package with models of neural networks used to train on pancreatic scans."""
from torch.nn import Module as Model

from ._resnet import (
    ResNet10,
    ResNet18,
    ResNet34,
    ResNet50,
    ResNet101,
    ResNet152,
    ResNet200,
)

__all__ = [
    "Model",
    "ResNet10",
    "ResNet18",
    "ResNet34",
    "ResNet50",
    "ResNet101",
    "ResNet152",
    "ResNet200",
]
