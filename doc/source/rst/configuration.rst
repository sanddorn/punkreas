******************
Configuration File
******************

In order to train with punkreas, you need a ``yaml`` configuration file.
The configuration file contains every setting for training, from the input
datasets to your model selection and configuration. In the following, we go
through the different options. 

It should be noted that relative paths in the configuration file are treated
as **relative to the configuration file** and not relative to the directory you
call punkreas from. This makes it easier to create folder structures where
the configuration file has a fixed relative position to i.e. the datasets
or the log folder. Of course, specifying absolute paths is allowed too.

Data Configuration
##################

The ``data`` section of the configuration file contains every setting related
to the selction, loading and transformation of the data. Under ``datasets``
you can specify an arbitrary number of datasets you want to train on. 

.. code-block:: yaml

    data:
      datasets:
        - ./path/to/dataset1
        - ./path/to/dataset2

All datasets are loaded and combined before transformation and training. Please
note: The datasets you specify here need to be in the correct format
as discussed in :doc:`preparation`. 

Next you can configure the allocation of your data between **training**,
**validation** and **testing** (see ``share`` option), the ``batch_size`` of
each subset's dataloader and whether the data in each subset should be
shuffled.

.. code-block:: yaml

    training:
      batch_size: 10
      share: 320
      shuffle: True
    validation:
      batch_size: 10
      share: 21
      shuffle: False
    test:
      batch_size: 10
      share: 20
      shuffle: False

The ``transform`` option allows you to specify data transformations. They are
executed in the exact order they are specified. In the example below,
each image would first be clipped and then normalized.
Configuration of the transformations works by passing the options you specify
directly to the respective transformation class in the :doc:`../punkreas.transform`.
Every class in the transform package can be used here just by using the class's
name as the key and the required or optional arguments as key value pairs.
In the example below :class:`.Resize` would be configured with with ``amin = -250``
and ``amax=-150`` and :class:`.Normalize` with ``bounds = [-1000, 3000]``.


.. code-block:: yaml

    transform:
      Clip:
        amin: -250
        amax: -150
      Normalize:
        bounds:
        - -250
        - -150

Loss, Metric and Optimizer Configuration
########################################

Similar to the transformation configuration in the data section, the loss
and metrics are selected by listing their class names, i.e. from the
:doc:`../punkreas.loss` for the loss, the :doc:`../punkreas.metrics`
for the metrics and the :doc:`../punkreas.optim` for the optimizer. In the
``metric`` section you can specify multiple metrics. Here is an example:

.. code-block:: yaml

    loss:
      BCELoss: {}
    metrics:
      Accuracy: {}
      Recall: {}
      Precision: {}
      F1: {}
      AUROC:
        num_classes: 1
    optimizer:
      StochasticGradientDescent:
        lr: 0.001

Model Configuration
###################

The model can be configured in two ways. You can either specify a model from
the :doc:`../punkreas.models` package like this:

.. code-block:: yaml

    model:
      ResNet10: {}

or you can import an existing model from a file like this:

.. code-block:: yaml

    model: "path/to/pretrained_model.pth"

Others
######

The sections above only cover the more complex settings. To create a complete
configuration file, you also need to specify the number of epochs under
``num_epochs`` and optionally a folder for the resulting model (``result_folder``) 
and a folder for the logging output (``log_folder``). Both can, however, be
deactivated by specifying ``Null`` as the value.

Optimize Parameters
###################

Punkreas has the optimizer package ``optuna`` built-in, so you can easily
create hyperparameter optimization sessions. There are only two things
you have to change in the configuration file in order for punkreas to
start the optimization process. First, you need to replace the parameter to optimize with
the key ``optimize`` followed by key-value pairs for the ``type``, the ``low``
value and the ``high`` value for the parameter: 

.. code-block:: yaml

    optimizer:
      StochasticGradientDescent:
        lr:
          optimize:
            type: float # or int
            low: 0.001
            high: 0.005

Morover, you need an additional section called ``optimize`` in the configuration
file with the following settings:

.. code-block:: yaml

    optimize:
      metric: "Accuracy/Validate"
      direction: "maximize"
      n_trials: 3
      timout: Null

Under ``metric``, you can choose every metric from the ``metrics`` section,
followed by a ``/Train``, ``/Validate`` or ``/Test``, depending on whether
you want to use the metric from the training, validation or test results,
respectively.