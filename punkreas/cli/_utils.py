"""This module contains utils for the punkreas.cli package."""
import shutil
from typing import Callable, Mapping, Optional, Union

import pytorch_lightning as pl
from matplotlib.figure import Figure
from torch import Tensor, cat, mean, nn, tensor

from ..optimizer import Optimizer


def print_full_terminal_width(text: str) -> None:
    """Print over full terminal width.

    Args:
        text: The text to print.
    """
    term_width = shutil.get_terminal_size(fallback=(80, 50)).columns
    text_width = len(text)
    fill_width = int((term_width - text_width) / 2) - 1
    print("=" * fill_width + " " + text + " " + "=" * fill_width)


def prune_epoch_results(results: list[dict]) -> dict:
    """Prune a list of result dicts to one dict with stacked data.

    Args:
        results: List of dictionaries with same keys.

    Returns:
        stacked_dict: Dictionary with the stacked data for each key.
    """
    pruned_results = {}
    for key in results[0].keys():
        if not results[0][key].shape:
            pruned_results[key] = tensor([result[key] for result in results])
        else:
            pruned_results[key] = cat([result[key] for result in results])
    return pruned_results


class TrainingBase(pl.LightningModule):
    """Base module for training."""

    def __init__(
        self,
        model: nn.Module,
        loss: Callable,
        optimizer: Callable,
        metrics: Optional[Mapping[str, nn.Module]] = None,
    ) -> None:
        """Create a new instance.

        Args:
            model: The model to train.
            loss: The loss function.
            optimizer: Training optimizer.
            metrics: Optional additional metrics for logging.
        """
        super(TrainingBase, self).__init__()

        self.model = model
        self.loss = loss
        if metrics:
            self.metrics = nn.ModuleDict(metrics)
        else:
            self.metrics = nn.ModuleDict({})
        self.optim = optimizer

    def forward(self, x: Tensor) -> Tensor:  # type: ignore
        """Defines the computation performed at every call.

        Args:
            x: Input tensor.

        Returns:
            y: Output tensor.
        """
        return self.model(x)

    def configure_optimizers(self) -> Optimizer:
        """Configure the optimizer for your classifier.

        Returns:
            optimizer: The optimizer for training.
        """
        return self.optim(self.parameters())

    def training_step(  # type: ignore
        self, batch: Tensor, batch_idx: int
    ) -> dict:
        """Training step.

        Computes and returns the training loss and some additional metrics
        for e.g. the progress bar or logger.

        Args:
            batch: The current batch tensor.
            batch_idx: Integer displaying index of this batch
        """
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        return {"loss": loss, "target": y.detach(), "pred": y_hat.detach()}

    def training_epoch_end(self, outputs: list[dict]) -> None:  # type: ignore
        """Training epoch end.

        Called at the end of the training epoch with the outputs of all
        training steps.

        Args:
            outputs: List of outputs of :meth:`training_step`, or if there are
                multiple dataloaders, a list containing a list of outputs for
                each dataloader.
        """
        pruned_results = prune_epoch_results(outputs)
        self.log("step", self.trainer.current_epoch)  # type: ignore
        self.log("Loss/Train", mean(pruned_results["loss"]))
        for name, func in self.metrics.items():
            self.log_metrics(
                name + "/Train",
                func(pruned_results["pred"], pruned_results["target"]),
            )

    def validation_step(  # type: ignore
        self, batch: Tensor, batch_idx: int
    ) -> dict:
        """Validation step.

        Computes and returns the validation loss and some additional metrics
        for e.g. the progress bar or logger.

        Args:
            batch: The current batch tensor.
            batch_idx: Integer displaying index of this batch
        """
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        return {"loss": loss, "target": y.detach(), "pred": y_hat.detach()}

    def validation_epoch_end(  # type: ignore
        self, outputs: list[dict]
    ) -> None:
        """Validation epoch end.

        Called at the end of the validation epoch with the outputs of all
        valdiation steps.

        Args:
            outputs: List of outputs of :meth:`validation_step`, or if there
                are multiple dataloaders, a list containing a list of outputs
                for each dataloader.
        """
        pruned_results = prune_epoch_results(outputs)
        self.log("step", self.trainer.current_epoch)  # type: ignore
        self.log("Loss/Validate", mean(pruned_results["loss"]))
        for name, func in self.metrics.items():
            self.log_metrics(
                name + "/Validate",
                func(pruned_results["pred"], pruned_results["target"]),
            )

    def test_step(self, batch: Tensor, batch_idx: int) -> dict:  # type: ignore
        """Test step.

        Computes and returns the test loss and some additional metrics
        for e.g. the progress bar or logger.

        Args:
            batch: The current batch tensor.
            batch_idx: Integer displaying index of this batch
        """
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        return {"loss": loss, "target": y.detach(), "pred": y_hat.detach()}

    def test_epoch_end(self, outputs: list[dict]) -> None:  # type: ignore
        """Test epoch end.

        Called at the end of the test epoch with the outputs of all test steps.

        Args:
            outputs: List of outputs of :meth:`test_step`, or if there are
                multiple dataloaders, a list containing a list of outputs for
                each dataloader.
        """
        pruned_results = prune_epoch_results(outputs)
        self.log("step", self.trainer.current_epoch)  # type: ignore
        self.log("Loss/Test", mean(pruned_results["loss"]))
        for name, func in self.metrics.items():
            self.log_metrics(
                name + "/Test",
                func(pruned_results["pred"], pruned_results["target"]),
            )

    def log_metrics(
        self, name: str, value: Union[int, float, Tensor, Figure]
    ) -> None:
        """Log metrics.

        Args:
            name: Key to log.
            value: Value to log.
        """
        if isinstance(value, Figure):
            self.logger.experiment.add_figure(
                name, value, global_step=self.current_epoch
            )
        else:
            super().log(name, value)  # type: ignore
