"""Tests for the punkreas.data.dataset._composition module."""
import pytest

from punkreas.data import DatasetComposition


@pytest.fixture
def test_dataset_composition(test_medical_image_dataset):
    """Test dataset composition."""
    return DatasetComposition(
        [test_medical_image_dataset, test_medical_image_dataset]
    )


def test_composition_init(
    test_dataset_composition, test_medical_image_dataset
):
    """Test the constructor."""
    assert test_dataset_composition._datasets == [
        test_medical_image_dataset,
        test_medical_image_dataset,
    ]
    assert test_dataset_composition._datasets_starts == [0, 2]


def test_composition_len(test_dataset_composition):
    """Test the __len__ method."""
    assert len(test_dataset_composition) == 4


def test_composition_getitem(mocker):
    """Test the __getitem__ method."""

    dataset1 = mocker.MagicMock()
    dataset2 = mocker.MagicMock()

    dataset1.__len__.return_value = 2
    dataset1.__getitem__.return_value = 123, 456
    dataset2.__getitem__.return_value = 234, 567
    dataset2.__len__.return_value = 3

    comp = DatasetComposition([dataset1, dataset2])

    assert comp[0] == (123, 456)
    assert comp[1] == (123, 456)
    assert comp[2] == (234, 567)
    assert comp[3] == (234, 567)
    assert comp[4] == (234, 567)
