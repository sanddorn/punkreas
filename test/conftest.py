"""Top-level pytest configuration file.

This file contains common functions and fixtures used by multiple tests.
"""
import os
from tempfile import TemporaryDirectory

import pytest

from punkreas.data import Dataset


@pytest.fixture
def testdata():
    """This fixture can be used to get path to the testdata folder."""
    current_dir = os.path.dirname(os.path.abspath(__file__))
    test_data_dir = os.path.join(current_dir, "ref")
    return test_data_dir


@pytest.fixture
def test_medical_image_dataset(testdata):
    """Test MedicalImageDataset object, created from a sample dataset."""

    return Dataset.from_compressed_folder(os.path.join(testdata, "dataset"))


@pytest.fixture
def tempdir():
    """Temporary directory for test purposes."""
    with TemporaryDirectory() as tempdir:
        yield tempdir
