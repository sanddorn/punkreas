"""This module holds the tests for the punkreas.data.image._scan module."""
import os

import numpy as np
import pytest

from punkreas.data import ScanImage


@pytest.fixture
def test_scan_image(testdata):
    """Test ScanImage object, created from an actual CT scan."""
    return ScanImage.from_path(
        os.path.join(testdata, "dataset", "scans", "000000.nii.gz"),
        dtype=np.float32,
    )


def test_scan_image_init(test_scan_image):
    """Test the constructor."""

    assert isinstance(test_scan_image._array, np.ndarray)
    assert test_scan_image._array.shape == (512, 512, 110)
    assert test_scan_image._array[0, 0, 0] == -1024.0
    assert test_scan_image._array[144, 270, 55] == 48.0
    assert test_scan_image._array[241, 294, 44] == 37.0


def test_scan_image_clip(test_scan_image):
    """Test the clip method."""

    test_scan_image.clip(40.0, 45.0)

    assert test_scan_image._array[144, 270, 55] == 45.0
    assert test_scan_image._array[241, 294, 44] == 40.0


def test_scan_image_normalize(test_scan_image):
    """Test the normalize method."""

    test_scan_image.normalize((-1000, 3000))

    assert np.isclose(test_scan_image._array[144, 270, 55], 0.262)
    assert np.isclose(test_scan_image._array[241, 294, 44], 0.25925)

    with pytest.raises(ValueError):
        test_scan_image.normalize((-1000, -1000))
