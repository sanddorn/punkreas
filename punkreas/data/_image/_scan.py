"""This module holds the ScanImage class."""
import numpy as np
import torch

from ._image import MedicalImage


class ScanImage(MedicalImage):
    """Scan Image.

    Medical image class for medical CT scans. Provides methods to read, modify
    and export 3D medical CT scans. The scan data itself is saved in form of
    a numpy array.

    Attributes:
        array: The array containing the scan data.
    """

    def to_torch(self) -> torch.Tensor:
        """Convert medical image to PyTorch tensor.

        Returns:
            img_tensor: The image tensor.
        """
        return torch.from_numpy(np.array([self._array]))

    def normalize(self, bounds: tuple[float, float]) -> None:
        """Normalize image data.

        Normalizes the image data based on the specified bounds.

        Args:
            bounds: Bounds for normalizing.
        """

        # Check if minimum and maximum value are not identical.
        if not bounds[1] - bounds[0] > 0:
            raise ValueError("Maximum bound <= minimum bound.")

        # Normalize image data array
        self._array = (
            (self._array - bounds[0]) / (bounds[1] - bounds[0])
        ).astype(np.float32)
