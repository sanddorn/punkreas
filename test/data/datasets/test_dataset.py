"""This module holds tests for the punkreas.data.dataset._dataset module."""
import os

import pytest
import torch

from punkreas.data import Dataset


def test_dataset_init(mocker, test_medical_image_dataset):
    """Test the constructor."""

    assert len(test_medical_image_dataset.scans) == 2
    assert len(test_medical_image_dataset.labels) == 2
    assert isinstance(test_medical_image_dataset.scans[0], str)
    assert isinstance(test_medical_image_dataset.scans[1], str)
    assert test_medical_image_dataset.labels[0] == 0
    assert test_medical_image_dataset.labels[1] == 1
    assert test_medical_image_dataset._transform is None

    transform_mock = mocker.Mock()
    dataset = Dataset([], [], transform=transform_mock)
    transform_mock.prepare.assert_called_once_with(dataset)

    with pytest.raises(ValueError):
        Dataset([], [1], transform=transform_mock)

    with pytest.raises(ValueError):
        Dataset([], [], transform=transform_mock, masks=["some/path"])


def test_dataset_len(test_medical_image_dataset):
    """Test the __len__ method."""
    assert len(test_medical_image_dataset) == 2


def test_dataset_getitem(mocker, test_medical_image_dataset):
    """Test the __getitem__ method."""

    scan, label = test_medical_image_dataset[0]

    assert isinstance(scan, torch.Tensor)
    assert scan.shape == torch.Size([1, 512, 512, 110])
    assert scan.dtype == torch.float32
    assert isinstance(label, torch.Tensor)
    assert label.shape == torch.Size([])
    assert label.dtype == torch.int64

    transform_mock = mocker.Mock()
    test_medical_image_dataset._transform = transform_mock
    test_medical_image_dataset[0]
    transform_mock.assert_called_once()


def test_dataset_from_compressed_folder(test_medical_image_dataset):
    """Test the from_compressed_folder method."""

    assert len(test_medical_image_dataset) == 2
    assert len(test_medical_image_dataset.scans) == 2
    assert len(test_medical_image_dataset.labels) == 2
    assert isinstance(test_medical_image_dataset.scans[0], str)
    assert isinstance(test_medical_image_dataset.scans[1], str)
    assert test_medical_image_dataset.labels[0] == 0
    assert test_medical_image_dataset.labels[1] == 1
    assert isinstance(
        test_medical_image_dataset.masks[0],
        str,
    )
    assert isinstance(
        test_medical_image_dataset.masks[1],
        str,
    )
    assert test_medical_image_dataset._transform is None


def test_dataset_to_compressed_folder(test_medical_image_dataset, tmpdir):
    """Test the from_compressed_folder method."""

    target = os.path.join(tmpdir, "targetfolder")

    test_medical_image_dataset.to_compressed_folder(target)

    created_files = os.listdir(target)

    assert len(created_files) == 3
    assert "scans" in created_files
    assert "masks" in created_files
    assert "labels.json" in created_files

    created_scans = os.listdir(os.path.join(target, "scans"))
    created_masks = os.listdir(os.path.join(target, "masks"))

    assert len(created_scans) == 2
    assert "000000.nii.gz" in created_scans
    assert "000001.nii.gz" in created_scans
    assert len(created_masks) == 2
    assert "000000.nii.gz" in created_masks
    assert "000001.nii.gz" in created_masks
