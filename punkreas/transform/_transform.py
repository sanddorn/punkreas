"""This module holds the abstract Transform class."""
import typing
from abc import ABC, abstractmethod

if typing.TYPE_CHECKING:
    from ..data import Dataset, ScanImage


class Transform(ABC):
    """Abstract base class for medical image transformation classes.

    This class defines all necessary interfaces for image transformation
    classes.
    """

    def __init__(self) -> None:
        """Create a new transformation instance.

        Please overwrite, if transformation needs to be configured when
        creating a new instance.
        """
        pass

    def prepare(self, dataset: "Dataset") -> None:
        """Prepare transformation instance based on the dataset.

        Please overwrite, if a transformation requires collecting prior
        information about a dataset. This method is called at initialization
        of a dataset.

        Args:
            dataset: Medical image dataset to prepare transformation for.
        """
        pass

    @abstractmethod
    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply transformation to image.

        Needs to be overwritten by and actual __call__ method.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        raise NotImplementedError
