*************
Installation
*************

After cloning this repository, ``punkreas`` and all its dependencies can be
installed easily via ``pip``. Just navigate to the root folder of the
repository and enter:

.. code-block:: bash

    pip install -e .


If you are contributing to punkreas, you can install punkreas 
**with development related dependencies**
using:

.. code-block:: bash

    pip install -e .[dev]
