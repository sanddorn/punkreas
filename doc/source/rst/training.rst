**************
Start Training
**************

With the configuration file ready and the datasets prepared as above, punkreas
can be executed using

.. code-block:: bash

    punkreas train [OPTIONS] CONFIG

wherein ``CONFIG`` is replaced by the path to the configuration file.
Valid options to punkreas are:

.. csv-table::
   :header: "Command", "Type", "Description"

   "``--num_workers``", Integer, "Number of workers for dataloader."
   "``--gpus``", Integer, "Number of gpus to train on or which GPUs to train on."
   "``--help``", Flag, "Show this message and exit."

There is also a batch mode available, which can be used with a folder of ``yaml``
configuration files as follows:

.. code-block:: bash

    punkreas batchtrain [OPTIONS] CONFIG_FOLDER