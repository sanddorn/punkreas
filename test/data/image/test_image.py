"""This module holds the tests for the punkreas.data.image._image module."""
import os

import numpy as np
import pytest
import SimpleITK as sitk

from punkreas.data import MedicalImage


@pytest.fixture
def dummy_medical_image():
    """A simple medical image instance."""
    MedicalImage.__abstractmethods__ = set()

    return MedicalImage(np.ones((3, 3, 3)))


def test_medical_image_init(dummy_medical_image):
    """Test the constructor."""

    assert isinstance(dummy_medical_image._array, np.ndarray)
    assert np.array_equal(dummy_medical_image._array, np.ones((3, 3, 3)))


def test_medical_image_getitem(dummy_medical_image):
    """Test the __getitem__ method."""
    assert dummy_medical_image[0, 1, 2] == 1
    sliced_image = dummy_medical_image[0:2, 1, 2]
    assert isinstance(sliced_image, MedicalImage)
    assert np.array_equal(sliced_image._array, np.array([1, 1]))


def test_medical_image_from_path(mocker):
    """Test the classmethod from_path."""

    with pytest.raises(ValueError):
        MedicalImage.from_path("does/not/exist", dtype=np.float32)

    mocker.patch("os.path.isfile", return_value=True)
    read_image_mock = mocker.patch(
        "SimpleITK.ReadImage",
        return_value=sitk.GetImageFromArray(np.ones((3, 3, 3))),
    )

    img = MedicalImage.from_path("some_path", dtype=np.float32)

    read_image_mock.assert_called_once_with(os.path.abspath("some_path"))

    assert isinstance(img, MedicalImage)
    assert np.array_equal(img._array, np.ones((3, 3, 3)))


def test_medical_image_array(dummy_medical_image):
    """Test the array property."""

    assert np.array_equal(
        dummy_medical_image.array, dummy_medical_image._array
    )


def test_medical_image_shape(dummy_medical_image):
    """Test the shape property."""
    assert dummy_medical_image.shape == (3, 3, 3)
