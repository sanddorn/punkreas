# punkreas

A generic framework for deep learning in healthcare imaging.

## Installation

After cloning this repository, punkreas and all its dependencies can be
installed easily via pip. Just navigate to the root folder of the repository and enter:

```bash
pip install -e .
```

If you are contributing to punkreas, you can install punkreas **with development related dependencies**
using:

```bash
pip install -e .[dev]
```

## Training

### Preparing the data

To use punkreas from the command line, the dataset must be in a specific
format. You can use punkreas to easily bring your dataset in the required
format. Just write a simple script as follows:

```python
from punkreas.data.datasets import Dataset

# Specify a target folder path+name for the dataset
target_folder = "path/to/folder"

# Write a script to read the filenames of the CT scans
scans: list[str] = ["my/first/scan", "my/second/scan"]

# Write a script to read/set the labels of the CT scans
labels: list = [1, 0]

# Optionally write a script to read the filenames of image masks
masks: list = ["my/first/mask", "my/second/mask"]

dataset = Dataset(scans, labels, masks=masks)
dataset.to_compressed_folder(target_folder)
```

### Training with punkreas

Punkreas can be called easily using the command line and a `yaml` configuration file.
Here is an example for such a configuration file.

```yaml
data:
  datasets:
    - path/to/dataset
  training:
    batch_size: 16
    share: 1030
    shuffle: True
  validation:
    batch_size: 16
    share: 265
    shuffle: False
  test:
    batch_size: 16
    share: 265
    shuffle: False
  transform:
    CropToMask:
      margin: 10
      min_dim: [50, 50, 50]
    Clip:
      amin: -150
      amax: 250
    Normalize:
      bounds:
      - -150
      - 250
    Pad:
      output_shape:
      - 271
      - 271
      - 271
    Resize:
      output_shape:
      - 128
      - 128
      - 128
    Albumentation:
      name: HorizontalFlip
      p: 0.5
    Albumentation:
      name: VerticalFlip
      p: 0.5
    Albumentation:
      name: RandomRotate90
      p: 0.5
    Albumentation:
      name: GridDistortion
      p: 0.5
    Albumentation:
      name: ElasticTransform
      p: 0.5
loss:
  BCELoss: {}
metrics:
  Accuracy:
    num_classes: 1
  Recall: {}
  Precision: {}
  F1: {}
  AUROC:
    num_classes: 1
  ConfusionMatrix:
    num_classes: 2
  Specificity: {}
model:
  ResNet10: {}
num_epochs: 100
optimizer:
  Adam:
    lr: 1.e-4
    weight_decay: 1.e-4
result_folder: "../results"
log_folder: "../logs"
seed: 0

```

It should be noted that relative paths in the configuration file are treated
as **relative to the configuration file** and not the current working directory.
Specifying absolute paths is, however, also supported.

With the configuration file ready and the datasets prepared as above, punkreas
can be executed using

```bash
punkreas train [OPTIONS] CONFIG
```
wherein `CONFIG` is replaced by the path to the configuration file.
Valid options to punkreas are:

```txt
--num_workers INTEGER  Number of workers for dataloader.  [default: 0]
--gpus INTEGER         Number of gpus to train on or which GPUs to train on.
--help                 Show this message and exit.
```

There is also a batch mode available, which can be used with a folder of `yaml`
configuration files as follows:

```bash
punkreas batchtrain [OPTIONS] CONFIG_FOLDER
```
