"""This module holds the Compose transformation class."""
import typing

from ._transform import Transform

if typing.TYPE_CHECKING:
    from ..data import Dataset, ScanImage


class Compose(Transform):
    """Composing multiple transformations to one transformation.

    This class allows composition of a new transformation based on applying
    multiple transformations in serial.
    """

    def __init__(self, transformations: list[Transform]) -> None:
        """Create a new transformation composition.

        Args:
            transformations: List of transformations applied in this
                composition.
        """
        self._transformations = transformations

    def prepare(self, dataset: "Dataset") -> None:
        """Prepare transformations based on the dataset.

        Args:
            dataset: Medical image dataset to prepare transformation for.
        """
        for transformation in self._transformations:
            transformation.prepare(dataset)

    def __call__(self, image: "ScanImage", idx: int) -> None:
        """Apply composition to image.

        Args:
            image: Medical image to transform.
            idx: Index of the medical image within the dataset.
        """
        for transformation in self._transformations:
            transformation(image, idx)
